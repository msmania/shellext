!IF "$(PLATFORM)"=="X64" || "$(PLATFORM)"=="x64"
ARCH=amd64
!ELSE
ARCH=x86
!ENDIF

OUTDIR=bin\$(ARCH)
OBJDIR=obj\$(ARCH)
SRCDIR=src

CC=cl
RD=rd/s/q
RM=del/q
LINKER=link
TARGET=z.dll

OBJS=\
	$(OBJDIR)\context.obj\
	$(OBJDIR)\copy.obj\
	$(OBJDIR)\data.obj\
	$(OBJDIR)\drop.obj\
	$(OBJDIR)\dllmain.obj\
	$(OBJDIR)\factory.obj\
	$(OBJDIR)\icon.obj\
	$(OBJDIR)\infotip.obj\
	$(OBJDIR)\meta.obj\
	$(OBJDIR)\overlay.obj\
	$(OBJDIR)\preview.obj\
	$(OBJDIR)\prop.obj\
	$(OBJDIR)\regutils.obj\
	$(OBJDIR)\thumb.obj\
	$(OBJDIR)\zzz.res\

LIBS=\
	advapi32.lib\
	comctl32.lib\
	shlwapi.lib\
	user32.lib\

CFLAGS=\
	/nologo\
	/c\
	/Od\
	/W4\
	/Zi\
	/EHsc\
	/DUNICODE\
	/D_UNICODE\
	/wd4100\
	/Fo"$(OBJDIR)\\"\
	/Fd"$(OBJDIR)\\"\

LFLAGS=\
	/NOLOGO\
	/DEBUG\
	/DLL\
	/DEF:$(SRCDIR)\zzz.def\
	/SUBSYSTEM:WINDOWS\
	/LIBPATH:"$(GTEST_BUILD_DIR)\lib\Release"\

all: $(OUTDIR)\$(TARGET)

$(OUTDIR)\$(TARGET): $(OBJS)
	@if not exist $(OUTDIR) mkdir $(OUTDIR)
	$(LINKER) $(LFLAGS) $(LIBS) /PDB:"$(@R).pdb" /OUT:$@ $**

{$(SRCDIR)}.cpp{$(OBJDIR)}.obj:
	@if not exist $(OBJDIR) mkdir $(OBJDIR)
	$(CC) $(CFLAGS) $<

{$(SRCDIR)}.rc{$(OBJDIR)}.res:
	@if not exist $(OBJDIR) mkdir $(OBJDIR)
	rc /nologo /fo "$@" $<

clean:
	@if exist $(OBJDIR) $(RD) $(OBJDIR)
	@if exist $(OUTDIR)\$(TARGET) $(RM) $(OUTDIR)\$(TARGET)
	@if exist $(OUTDIR)\$(TARGET:dll=exp) $(RM) $(OUTDIR)\$(TARGET:dll=exp)
	@if exist $(OUTDIR)\$(TARGET:dll=ilk) $(RM) $(OUTDIR)\$(TARGET:dll=ilk)
	@if exist $(OUTDIR)\$(TARGET:dll=lib) $(RM) $(OUTDIR)\$(TARGET:dll=lib)
	@if exist $(OUTDIR)\$(TARGET:dll=pdb) $(RM) $(OUTDIR)\$(TARGET:dll=pdb)
