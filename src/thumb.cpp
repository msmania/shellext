#include <windows.h>
#include <shlwapi.h>
#include <shlobj.h>
#include <strsafe.h>
#include <thumbcache.h>

extern void Log(LPCWSTR format, ...);

class ThumbExt : public IInitializeWithStream,
                 public IThumbnailProvider {
  ULONG ref_;

public:
  ThumbExt() : ref_(1) {
    Log(L"ThumbExt: %p\n", this);
  }

  STDMETHODIMP QueryInterface(REFIID riid, void **ppv) {
    const QITAB QITable[] = {
      QITABENT(ThumbExt, IInitializeWithStream),
      QITABENT(ThumbExt, IThumbnailProvider),
      {0},
    };
    return QISearch(this, QITable, riid, ppv);
  }
  STDMETHODIMP_(ULONG) AddRef() { return InterlockedIncrement(&ref_); }
  STDMETHODIMP_(ULONG) Release() {
    auto cref = InterlockedDecrement(&ref_);
    if (cref == 0) {
      Log(L"Destroying ThumbExt %p\n", this);
      delete this;
    }
    return cref;
  }

  // IInitializeWithStream
  STDMETHODIMP Initialize(IStream *pstream, DWORD grfMode) {
    return S_OK;
  }

  // IThumbnailProvider
  STDMETHODIMP GetThumbnail(UINT cx, HBITMAP *phbmp, WTS_ALPHATYPE *pdwAlpha) {
    Log(L"IThumbnailProvider::GetThumbnail\n");
    return E_NOTIMPL;
  }
};

IThumbnailProvider* CreateThumbExt() { return new ThumbExt; }
