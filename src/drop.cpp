#include <memory>
#include <windows.h>
#include <shlwapi.h>
#include <shlobj.h>
#include <strsafe.h>

extern GUID CLSID_ExtZ;
extern void Log(LPCWSTR format, ...);

class DropExt : public IPersistFile, public IDropTarget {
  ULONG ref_;

public:
  DropExt() : ref_(1) {
    Log(L"DropExt: %p\n", this);
  }

  STDMETHODIMP QueryInterface(REFIID riid, void **ppv) {
    const QITAB QITable[] = {
      QITABENT(DropExt, IPersist),
      QITABENT(DropExt, IPersistFile),
      QITABENT(DropExt, IDropTarget),
      {0},
    };
    return QISearch(this, QITable, riid, ppv);
  }
  STDMETHODIMP_(ULONG) AddRef() { return InterlockedIncrement(&ref_); }
  STDMETHODIMP_(ULONG) Release() {
    auto cref = InterlockedDecrement(&ref_);
    if (cref == 0) {
      Log(L"Destroying DropExt %p\n", this);
      delete this;
    }
    return cref;
  }

  // IPersist
  STDMETHODIMP GetClassID(CLSID *pClassID) {
    *pClassID = CLSID_ExtZ;
    return S_OK;
  }

  // IPersistFile
  STDMETHODIMP GetCurFile(LPOLESTR *ppszFileName) {
    Log(L"IPersistFile::GetCurFile\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP IsDirty() { return S_FALSE; }
  STDMETHODIMP Load(LPCOLESTR pszFileName, DWORD dwMode) {
    Log(L"IPersistFile::Load %p %d %s\n", this, dwMode, pszFileName);
    return E_NOTIMPL;
  }
  STDMETHODIMP Save(LPCOLESTR pszFileName, BOOL fRemember) {
    Log(L"IPersistFile::Save\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP SaveCompleted(LPCOLESTR pszFileName) {
    Log(L"IPersistFile::SaveCompleted\n");
    return E_NOTIMPL;
  }

  // IDropTarget
  STDMETHODIMP DragEnter(IDataObject *pDataObj,
                         DWORD grfKeyState,
                         POINTL pt,
                         DWORD *pdwEffect) {
    Log(L"IDropTarget::DragEnter\n");
    return E_NOTIMPL;
  }

  STDMETHODIMP DragOver(DWORD grfKeyState, POINTL pt, DWORD *pdwEffect) {
    Log(L"IDropTarget::DragOver\n");
    return E_NOTIMPL;
  }

  STDMETHODIMP DragLeave() {
    Log(L"IDropTarget::DragLeave\n");
    return E_NOTIMPL;
  }

  STDMETHODIMP Drop(IDataObject *pDataObj,
                    DWORD grfKeyState,
                    POINTL pt,
                    DWORD *pdwEffect) {
    Log(L"IDropTarget::Drop\n");
    return E_NOTIMPL;
  }
};

IDropTarget* CreateDropExt() {
  return new DropExt;
}
