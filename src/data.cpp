#include <memory>
#include <windows.h>
#include <shlwapi.h>
#include <shlobj.h>
#include <strsafe.h>

extern GUID CLSID_ExtZ;
extern void Log(LPCWSTR format, ...);

class DataExt : public IPersistFile, public IDataObject {
  ULONG ref_;

public:
  DataExt() : ref_(1) {
    Log(L"DataExt: %p\n", this);
  }

  STDMETHODIMP QueryInterface(REFIID riid, void **ppv) {
    const QITAB QITable[] = {
      QITABENT(DataExt, IPersist),
      QITABENT(DataExt, IPersistFile),
      QITABENT(DataExt, IDataObject),
      {0},
    };
    return QISearch(this, QITable, riid, ppv);
  }
  STDMETHODIMP_(ULONG) AddRef() { return InterlockedIncrement(&ref_); }
  STDMETHODIMP_(ULONG) Release() {
    auto cref = InterlockedDecrement(&ref_);
    if (cref == 0) {
      Log(L"Destroying DataExt %p\n", this);
      delete this;
    }
    return cref;
  }

  // IPersist
  STDMETHODIMP GetClassID(CLSID *pClassID) {
    *pClassID = CLSID_ExtZ;
    return S_OK;
  }

  // IPersistFile
  STDMETHODIMP GetCurFile(LPOLESTR *ppszFileName) {
    Log(L"IPersistFile::GetCurFile\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP IsDirty() { return S_FALSE; }
  STDMETHODIMP Load(LPCOLESTR pszFileName, DWORD dwMode) {
    Log(L"IPersistFile::Load %p %d %s\n", this, dwMode, pszFileName);
    return E_NOTIMPL;
  }
  STDMETHODIMP Save(LPCOLESTR pszFileName, BOOL fRemember) {
    Log(L"IPersistFile::Save\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP SaveCompleted(LPCOLESTR pszFileName) {
    Log(L"IPersistFile::SaveCompleted\n");
    return E_NOTIMPL;
  }

  // IDataObject
  STDMETHODIMP GetData(FORMATETC *pformatetcIn, STGMEDIUM *pmedium) {
    Log(L"IDataObject::GetData\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP GetDataHere(FORMATETC *pformatetc, STGMEDIUM *pmedium) {
    Log(L"IDataObject::GetDataHere\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP QueryGetData(FORMATETC *pformatetc) {
    Log(L"IDataObject::QueryGetData\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP GetCanonicalFormatEtc(FORMATETC *pformatectIn, FORMATETC *pformatetcOut) {
    Log(L"IDataObject::GetCanonicalFormatEtc\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP SetData(FORMATETC *pformatetc, STGMEDIUM *pmedium, BOOL fRelease) {
    Log(L"IDataObject::SetData\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP EnumFormatEtc(DWORD dwDirection, IEnumFORMATETC **ppenumFormatEtc) {
    Log(L"IDataObject::EnumFormatEtc\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP DAdvise(FORMATETC *pformatetc, DWORD advf, IAdviseSink *pAdvSink, DWORD *pdwConnection) {
    Log(L"IDataObject::DAdvise\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP DUnadvise(DWORD dwConnection) {
    Log(L"IDataObject::DUnadvise\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP EnumDAdvise(IEnumSTATDATA **ppenumAdvise) {
    Log(L"IDataObject::EnumDAdvise\n");
    return E_NOTIMPL;
  }
};

IDataObject* CreateDataExt() {
  return new DataExt;
}
