#include <atlbase.h>
#include <windows.h>
#include <strsafe.h>

#include "regutils.h"

extern IUnknown* CreateZzzFactory();

const wchar_t kFriendlyName[] = L"ZzzComObject";
std::wstring gDllPath;
HMODULE gThisModule = nullptr;
UINT gRefCount = 0;

// {879E6C8C-8599-4FF4-92C9-FB819F18C39F}
GUID CLSID_ExtZ =
{ 0x879e6c8c, 0x8599, 0x4ff4, { 0x92, 0xc9, 0xfb, 0x81, 0x9f, 0x18, 0xc3, 0x9f } };

void Log(LPCWSTR format, ...) {
  WCHAR linebuf[1024];
  va_list v;
  va_start(v, format);
  StringCbVPrintf(linebuf, sizeof(linebuf), format, v);
  va_end(v);
  OutputDebugString(linebuf);
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD dwReason, LPVOID) {
  wchar_t buf[MAX_PATH];
  switch (dwReason) {
  case DLL_PROCESS_ATTACH:
    gThisModule = hModule;
    if (!GetModuleFileName(hModule, buf, ARRAYSIZE(buf))) {
      return FALSE;
    }
    gDllPath = buf;
    break;
  case DLL_THREAD_ATTACH:
  case DLL_THREAD_DETACH:
  case DLL_PROCESS_DETACH:
    break;
  }
  return TRUE;
}

STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, void **ppv) {
  if (!IsEqualCLSID(rclsid, CLSID_ExtZ)
      || !IsEqualCLSID(riid, IID_IClassFactory))
    return CLASS_E_CLASSNOTAVAILABLE;

  CComPtr<IUnknown> factory;
  factory.Attach(CreateZzzFactory());
  return factory ? factory->QueryInterface(riid, ppv)
                 : E_OUTOFMEMORY;
}

STDAPI DllCanUnloadNow() {
  return S_OK;
}

STDAPI DllRegisterServer() {
  // https://docs.microsoft.com/en-us/windows/win32/shell/reg-shell-exts
  ComRegistry reg(CLSID_ExtZ, kFriendlyName);
  if (!reg.RegisterObject(L"Apartment")
      || !reg.RegisterExtensions()) {
    return E_ACCESSDENIED;
  }

  return S_OK;
}

STDAPI DllUnregisterServer() {
  ComRegistry reg(CLSID_ExtZ, kFriendlyName);
  if (!reg.UnregisterAll()) {
    return E_ACCESSDENIED;
  }

  return S_OK;
}
