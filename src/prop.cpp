#include <memory>
#include <windows.h>
#include <shlwapi.h>
#include <shlobj.h>
#include <strsafe.h>

#include "res.h"

extern HMODULE gThisModule;
extern UINT gRefCount;

extern void Log(LPCWSTR format, ...);
extern void PrintInfo(PCIDLIST_ABSOLUTE pidlFolder, IDataObject *pdtobj, HKEY hkeyProgID);

class PropExt : public IShellExtInit, public IShellPropSheetExt {
  static UINT WINAPI PageCallbackProc(HWND hwnd, UINT msg, PROPSHEETPAGEW *ppsp) {
    switch (msg) {
      case PSPCB_CREATE: return 1;
      case PSPCB_ADDREF:
        reinterpret_cast<PropExt*>(ppsp->lParam)->AddRef();
        break;
      case PSPCB_RELEASE:
        reinterpret_cast<PropExt*>(ppsp->lParam)->Release();
        break;
    }
    return 0;
  }

  static INT_PTR WINAPI PageDlgProc(HWND hwnd, UINT msg, WPARAM w, LPARAM l) {
    switch (msg) {
      case WM_INITDIALOG:
        return TRUE;
      case WM_NOTIFY:
        Log(L"PageDlgProc(WM_NOTIFY): %d\n", reinterpret_cast<LPNMHDR>(l)->code);
        break;
    }
    return 0;
  }

  ULONG ref_;

public:
  PropExt() : ref_(1) {
    Log(L"PropExt: %p\n", this);
  }

  STDMETHODIMP QueryInterface(REFIID riid, void **ppv) {
    const QITAB QITable[] = {
      QITABENT(PropExt, IShellExtInit),
      QITABENT(PropExt, IShellPropSheetExt),
      {0},
    };
    return QISearch(this, QITable, riid, ppv);
  }
  STDMETHODIMP_(ULONG) AddRef() { return InterlockedIncrement(&ref_); }
  STDMETHODIMP_(ULONG) Release() {
    auto cref = InterlockedDecrement(&ref_);
    if (cref == 0) {
      Log(L"Destroying PropExt %p\n", this);
      delete this;
    }
    return cref;
  }

  // IShellExtInit
  STDMETHODIMP Initialize(PCIDLIST_ABSOLUTE pidlFolder,
                          IDataObject *pdtobj,
                          HKEY hkeyProgID) {
    PrintInfo(pidlFolder, pdtobj, hkeyProgID);
    return S_OK;
  }

  // IShellPropSheetExt
  STDMETHODIMP AddPages(LPFNSVADDPROPSHEETPAGE pfnAddPage, LPARAM lParam) {
    PROPSHEETPAGE psp = {sizeof(psp)};
    psp.dwFlags = PSP_USEREFPARENT | PSP_USETITLE | PSP_USECALLBACK;
    psp.hInstance = gThisModule;
    psp.pszTemplate = MAKEINTRESOURCE(IDD_PAGEDLG);
    psp.hIcon = 0;
    psp.pszTitle = L":)";
    psp.pfnDlgProc = PageDlgProc;
    psp.pcRefParent = &gRefCount;
    psp.pfnCallback = PageCallbackProc;
    psp.lParam = reinterpret_cast<LPARAM>(this);

    HPROPSHEETPAGE hPage = CreatePropertySheetPage(&psp);
    if(!hPage) {
      Log(L"CreatePropertySheetPage failed - %08x\n", GetLastError());
      return E_FAIL;
    }

    if (!pfnAddPage(hPage, lParam)) {
      DestroyPropertySheetPage(hPage);
      Log(L"pfnAddPage failed - %08x\n", GetLastError());
      return E_FAIL;
    }

    return S_OK;
  }

  STDMETHODIMP ReplacePage(EXPPS uPageID,
                           LPFNSVADDPROPSHEETPAGE pfnReplaceWith,
                           LPARAM lParam) {
    return E_NOTIMPL;
  }
};

IShellPropSheetExt* CreatePropExt() {
  return new PropExt;
}
