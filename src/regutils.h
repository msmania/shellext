#pragma once

#include <string>

class RegUtil final {
  HKEY key_;

  void Release();
  bool SetStringInternal(LPCWSTR valueName,
                         LPCWSTR valueData,
                         DWORD valueDataLength);

 public:
  static std::wstring GuidToString(const GUID& guid);

  RegUtil();
  RegUtil(HKEY root, LPCWSTR subkey);
  RegUtil(RegUtil&& other);
  RegUtil& operator=(RegUtil&& other);

  RegUtil(const RegUtil&) = delete;
  RegUtil& operator=(const RegUtil&) = delete;

  ~RegUtil();

  operator bool() const;
  operator HKEY() const;

  bool SetString(LPCWSTR valueName, LPCWSTR valueData = nullptr);
  bool SetString(LPCWSTR valueName, const std::wstring& valueData);

  std::wstring GetString(LPCWSTR valueName);
};

class ComRegistry final {
public:
  enum RegType {Hive, ClassRoot};

private:
  GUID clsId_;
  std::wstring name_;
  RegUtil hiveRoot_;
  RegUtil classRoot_;

  HKEY GetHKey(RegType type);

public:
  ComRegistry(const GUID &clsId, LPCWSTR name);
  ~ComRegistry() = default;

  bool UnregisterAll();
  bool RegisterObject(LPCWSTR threadModel);
  bool RegisterExtensions();
};
