#pragma once

#define IDI_ICON1 100
#define IDI_ICON2 101
#define IDD_PAGEDLG 1000

#define VER_FILEVERSION         1,2,3,4
#define VER_FILEVERSION_STR    "2.0.0.0\0"
#define VER_PRODUCTVERSION      3,0,0,0
#define VER_PRODUCTVERSION_STR "4.0.0.0\0"
