#include <string>
#include <windows.h>
#include <shlwapi.h>
#include <shlobj.h>
#include <strsafe.h>

extern std::wstring gDllPath;
extern void Log(LPCWSTR format, ...);

class OverlayIconExt : public IShellIconOverlayIdentifier {
  ULONG ref_;

public:
  OverlayIconExt();

  STDMETHODIMP QueryInterface(REFIID riid, void **ppv);
  STDMETHODIMP_(ULONG) AddRef();
  STDMETHODIMP_(ULONG) Release();

  // IShellIconOverlayIdentifier
  STDMETHODIMP GetOverlayInfo(LPWSTR pwszIconFile,
                              int cchMax,
                              int *pIndex,
                              DWORD *pdwFlags);
  STDMETHODIMP GetPriority(int* pPriority);
  STDMETHODIMP IsMemberOf(LPCWSTR pwszPath, DWORD dwAttrib);
};

OverlayIconExt::OverlayIconExt() : ref_(1) {
  Log(L"OverlayIconExt: %p\n", this);
}

STDMETHODIMP OverlayIconExt::QueryInterface(REFIID riid, void **ppv) {
  const QITAB QITable[] = {
    QITABENT(OverlayIconExt, IShellIconOverlayIdentifier),
    {0},
  };
  return QISearch(this, QITable, riid, ppv);
}

STDMETHODIMP_(ULONG) OverlayIconExt::AddRef() {
  return InterlockedIncrement(&ref_);
}

STDMETHODIMP_(ULONG) OverlayIconExt::Release() {
  auto cref = InterlockedDecrement(&ref_);
  if (cref == 0) {
    Log(L"Destroying OverlayIconExt %p\n", this);
    delete this;
  }
  return cref;
}

STDMETHODIMP OverlayIconExt::GetOverlayInfo(LPWSTR pwszIconFile,
                                            int cchMax,
                                            int *pIndex,
                                            DWORD *pdwFlags) {
  HRESULT hr = StringCchCopy(pwszIconFile, cchMax, gDllPath.c_str());
  if (FAILED(hr)) return hr;

  *pIndex = 0;
  *pdwFlags = ISIOI_ICONFILE | ISIOI_ICONINDEX;
  return S_OK;
}

STDMETHODIMP OverlayIconExt::GetPriority(int* pPriority) {
  *pPriority = 0;
  return S_OK;
}

STDMETHODIMP OverlayIconExt::IsMemberOf(LPCWSTR pwszPath, DWORD dwAttrib) {
  return S_OK;
}

IUnknown* CreateOverlayIconExt() {
  return new OverlayIconExt;
}
