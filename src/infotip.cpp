#include <windows.h>
#include <shlwapi.h>
#include <shlobj.h>
#include <strsafe.h>
#include <thumbcache.h>

extern GUID CLSID_ExtZ;
extern void Log(LPCWSTR format, ...);
/*
IInitializeWithStream
IInitializeWithItem
IInitializeWithFile

IID_IPersistStream
IParentAndItem
IPersistFile
*/
class InfotipExt : public IInitializeWithStream,
                   public IPersistFile,
                   public IQueryInfo {
  ULONG ref_;

public:
  InfotipExt() : ref_(1) {
    Log(L"InfotipExt: %p\n", this);
  }

  STDMETHODIMP QueryInterface(REFIID riid, void **ppv) {
    const QITAB QITable[] = {
      QITABENT(InfotipExt, IInitializeWithStream),
      QITABENT(InfotipExt, IPersistFile),
      QITABENT(InfotipExt, IQueryInfo),
      {0},
    };
    return QISearch(this, QITable, riid, ppv);
  }
  STDMETHODIMP_(ULONG) AddRef() { return InterlockedIncrement(&ref_); }
  STDMETHODIMP_(ULONG) Release() {
    auto cref = InterlockedDecrement(&ref_);
    if (cref == 0) {
      Log(L"Destroying InfotipExt %p\n", this);
      delete this;
    }
    return cref;
  }

  // IInitializeWithStream
  STDMETHODIMP Initialize(IStream *pstream, DWORD grfMode) {
    return S_OK;
  }

  // IPersist
  STDMETHODIMP GetClassID(CLSID *pClassID) {
    *pClassID = CLSID_ExtZ;
    return S_OK;
  }

  // IPersistFile
  STDMETHODIMP GetCurFile(LPOLESTR *ppszFileName) { return E_NOTIMPL; }
  STDMETHODIMP IsDirty() { return S_FALSE; }
  STDMETHODIMP Load(LPCOLESTR pszFileName, DWORD dwMode) { return E_NOTIMPL; }
  STDMETHODIMP Save(LPCOLESTR pszFileName, BOOL fRemember) { return E_NOTIMPL; }
  STDMETHODIMP SaveCompleted(LPCOLESTR pszFileName) { return E_NOTIMPL; }

  // IQueryInfo
  STDMETHODIMP GetInfoFlags(DWORD *pdwFlags) {
    *pdwFlags = 0;
    return S_OK;
  }
  STDMETHODIMP GetInfoTip(DWORD dwFlags, PWSTR *ppwszTip) {
    if (!ppwszTip) return E_INVALIDARG;

    const wchar_t kTip[] = L":)";
    *ppwszTip = reinterpret_cast<wchar_t*>(CoTaskMemAlloc(sizeof(kTip)));
    if (!(*ppwszTip)) return E_OUTOFMEMORY;

    memcpy(*ppwszTip, kTip, sizeof(kTip));
    return S_OK;
  }
};

IQueryInfo* CreateInfotipExt() { return new InfotipExt; }
