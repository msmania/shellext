#include <memory>
#include <windows.h>
#include <strsafe.h>

#include "regutils.h"

extern std::wstring gDllPath;
extern void Log(LPCWSTR format, ...);

void RegUtil::Release() {
  if (!key_) return;

  LSTATUS ls = RegCloseKey(key_);
  if (ls != ERROR_SUCCESS) {
    Log(L"RegCloseKey failed - %08x\n", ls);
  }
}

bool RegUtil::SetStringInternal(LPCWSTR valueName,
                                LPCWSTR valueData,
                                DWORD valueDataLength) {
  if (!key_) return false;

  LSTATUS ls = RegSetValueEx(key_,
                             valueName,
                             /*Reserved*/0,
                             REG_SZ,
                             reinterpret_cast<const BYTE*>(valueData),
                             valueDataLength);
  if (ls != ERROR_SUCCESS) {
    Log(L"RegSetValueEx failed - %08x\n", ls);
    return false;
  }
  return true;
}

std::wstring RegUtil::GuidToString(const GUID& guid) {
  wchar_t guidStr[64];
  HRESULT hr = StringCbPrintf(
      guidStr, sizeof(guidStr),
      L"{%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x}",
      guid.Data1,
      guid.Data2,
      guid.Data3,
      guid.Data4[0],
      guid.Data4[1],
      guid.Data4[2],
      guid.Data4[3],
      guid.Data4[4],
      guid.Data4[5],
      guid.Data4[6],
      guid.Data4[7]);
  if (FAILED(hr)) {
    Log(L"StringCbPrintf failed - %08x\n", hr);
    return L"";
  }
  return guidStr;
}

RegUtil::RegUtil() : key_(nullptr) {}

RegUtil::RegUtil(HKEY root, LPCWSTR subkey) : key_(nullptr) {
  DWORD dispo;
  LSTATUS ls = RegCreateKeyExW(root,
                               subkey,
                               /*Reserved*/0,
                               /*lpClass*/nullptr,
                               /*dwOptions*/0,
                               KEY_ALL_ACCESS,
                               /*lpSecurityAttributes*/nullptr,
                               &key_,
                               &dispo);
  if (ls != ERROR_SUCCESS) {
    Log(L"RegCreateKeyExW failed - %08x\n", ls);
    return;
  }
}

RegUtil::RegUtil(RegUtil&& other) : key_(other.key_) {
  other.key_ = nullptr;
}

RegUtil& RegUtil::operator=(RegUtil&& other) {
  if (this != &other) {
    Release();
    key_ = other.key_;
    other.key_ = nullptr;
  }
  return *this;
}

RegUtil::~RegUtil() { Release(); }

RegUtil::operator bool() const { return !!key_; }
RegUtil::operator HKEY() const { return key_; }

bool RegUtil::SetString(LPCWSTR valueName, LPCWSTR valueData) {
  return SetStringInternal(
      valueName, valueData,
      valueData
          ? static_cast<DWORD>((wcslen(valueData) + 1) * sizeof(wchar_t))
          : 0);
}

bool RegUtil::SetString(LPCWSTR valueName, const std::wstring& valueData) {
  return SetStringInternal(
      valueName, valueData.c_str(),
      static_cast<DWORD>((valueData.size() + 1) * sizeof(wchar_t)));
}

std::wstring RegUtil::GetString(LPCWSTR valueName) {
  DWORD type;
  for (DWORD len = 1; ; len *= 2) {
    std::unique_ptr<uint8_t[]> buf(new uint8_t[len]);
    LSTATUS status = ::RegGetValueW(key_, valueName, nullptr,
                                    RRF_RT_REG_SZ, &type, buf.get(), &len);
    if (status == ERROR_SUCCESS) {
      return std::wstring(reinterpret_cast<wchar_t*>(buf.get()));
    }
    else if (status == ERROR_FILE_NOT_FOUND) {
      return L"";
    }
    else if (status != ERROR_MORE_DATA) {
      Log(L"RegGetValueW failed - %08x\n", status);
      return L"";
    }
  }
}

HKEY ComRegistry::GetHKey(RegType type) {
  switch (type) {
    case Hive: return hiveRoot_;
    case ClassRoot: return classRoot_;
    default: return nullptr;
  }
}

ComRegistry::ComRegistry(const GUID &clsId, LPCWSTR name)
  : clsId_(clsId), name_(name) {
#if 0
  hiveRoot_ = RegUtil(HKEY_LOCAL_MACHINE, L"");
  classRoot_ = RegUtil(HKEY_CLASSES_ROOT, L"");
#else
  hiveRoot_ = RegUtil(HKEY_CURRENT_USER, L"");
  classRoot_ = RegUtil(HKEY_CURRENT_USER, L"Software\\Classes");
#endif
}

const wchar_t kClsIdPrefix[] = L"CLSID\\";
const wchar_t* kExts[] = { L".zzz" };

struct ExtensionConfig {
  ComRegistry::RegType mType;
  const wchar_t* mSubkeyPrefix;
} kExtensions[] = {
  ComRegistry::Hive, L"Software\\Microsoft\\Windows\\CurrentVersion\\"
                     L"Explorer\\ShellIconOverlayIdentifiers\\",

  ComRegistry::ClassRoot, L".zzz\\shellex\\ContextMenuHandlers\\",
  ComRegistry::ClassRoot, L"Directory\\shellex\\ContextMenuHandlers\\",
  ComRegistry::ClassRoot, L"Directory\\Background\\shellex\\ContextMenuHandlers\\",
  ComRegistry::ClassRoot, L"Folder\\shellex\\ContextMenuHandlers\\",

  ComRegistry::ClassRoot, L".zzz\\shellex\\CopyHookHandlers\\", // invalid
  ComRegistry::ClassRoot, L"Directory\\shellex\\CopyHookHandlers\\",
  ComRegistry::ClassRoot, L"Folder\\shellex\\CopyHookHandlers\\", // invalid
  ComRegistry::ClassRoot, L"Printers\\shellex\\CopyHookHandlers\\",

  ComRegistry::ClassRoot, L".zzz\\shellex\\DragDropHandlers\\",
  ComRegistry::ClassRoot, L"Directory\\shellex\\DragDropHandlers\\",
  ComRegistry::ClassRoot, L"Drive\\shellex\\DragDropHandlers\\",
  ComRegistry::ClassRoot, L"Folder\\shellex\\DragDropHandlers\\",

  ComRegistry::ClassRoot, L".zzz\\shellex\\PropertySheetHandlers\\",
  ComRegistry::ClassRoot, L"Directory\\shellex\\PropertySheetHandlers\\",
  ComRegistry::ClassRoot, L"Folder\\shellex\\PropertySheetHandlers\\",

  ComRegistry::ClassRoot, L".zzz\\shellex\\DataHandler",
  ComRegistry::ClassRoot, L".zzz\\shellex\\DropHandler",
  ComRegistry::ClassRoot, L".zzz\\shellex\\IconHandler",
  ComRegistry::ClassRoot, L".zzz\\shellex\\PropertyHandler",
  // Infotip handler
  ComRegistry::ClassRoot, L".zzz\\shellex\\{00021500-0000-0000-C000-000000000046}",

  // Preview Handler (Out-of-process)
  ComRegistry::ClassRoot, L".zzz\\shellex\\{8895b1c6-b41f-4c1c-a562-0d564250836f}",
  // Thumbnail Handler (Out-of-process)
  ComRegistry::ClassRoot, L".zzz\\shellex\\{E357FCCD-A995-4576-B01F-234630154E96}",

  // Pin to Start Menu
  ComRegistry::ClassRoot, L".zzz\\shellex\\{a2a9545d-a0c2-42b4-9708-a0b2badd77c8}",
  // Pin to Taskbar
  ComRegistry::ClassRoot, L".zzz\\shellex\\{90AA3A4E-1CBA-4233-B8BB-535773D48449}",

  // XP or older?
  ComRegistry::ClassRoot, L"Folder\\shellex\\ColumnHandlers\\",
  ComRegistry::Hive,      L"Software\\Microsoft\\Windows\\CurrentVersion\\"
                          L"Explorer\\FindExtensions\\",
};
constexpr size_t kNumExtensions = sizeof(kExtensions) / sizeof(ExtensionConfig);

const wchar_t kPrevhost32[] = L"{534A1E02-D58F-44f0-B58B-36CBED287C7C}";
const wchar_t kPrevhost64[] = L"{6d2b5079-2f0b-48dd-ab7f-97cec514d30b}";
const wchar_t kPreviewList[] = L"Software\\Microsoft\\Windows\\CurrentVersion\\"
                               L"PreviewHandlers";
const wchar_t kPropHandlerList[]
    = L"Software\\Microsoft\\Windows\\CurrentVersion\\"
      L"PropertySystem\\PropertyHandlers\\";

bool ComRegistry::UnregisterAll() {
  bool isOk = true;
  LSTATUS ls;
  std::wstring subkey;

  const auto guidStr = RegUtil::GuidToString(clsId_);

  for (int i = kNumExtensions - 1; i >= 0; --i) {
    subkey = kExtensions[i].mSubkeyPrefix;

    if (subkey[subkey.size() - 1] == L'\\') {
      subkey += guidStr;

      ls = RegDeleteTree(GetHKey(kExtensions[i].mType), subkey.c_str());
      if (ls != ERROR_SUCCESS && ls != ERROR_FILE_NOT_FOUND) {
        Log(L"RegDeleteTree(%s) failed - %08x\n", subkey.c_str(), ls);
        isOk = false;
      }
    }
    else {
      RegUtil root(GetHKey(kExtensions[i].mType), subkey.c_str());
      auto handlerName = root.GetString(nullptr);
      if (handlerName != guidStr) continue;
      if (!root.SetString(nullptr))
        isOk = false;
    }
  }

  RegUtil root(GetHKey(Hive), kPreviewList);
  ls = RegDeleteValueW(root, guidStr.c_str());
  if (ls != ERROR_SUCCESS && ls != ERROR_FILE_NOT_FOUND) {
    Log(L"RegDeleteValueW(%s) failed - %08x\n", subkey.c_str(), ls);
    isOk = false;
  }

  for (const wchar_t* ext : kExts) {
    subkey = kPropHandlerList;
    subkey += ext;

    ls = RegDeleteTree(GetHKey(Hive), subkey.c_str());
    if (ls != ERROR_SUCCESS && ls != ERROR_FILE_NOT_FOUND) {
      Log(L"RegDeleteTree failed - %08x\n", ls);
      isOk = false;
    }
  }

  subkey = kClsIdPrefix;
  subkey += guidStr;
  ls = RegDeleteTree(GetHKey(ClassRoot), subkey.c_str());
  if (ls != ERROR_SUCCESS && ls != ERROR_FILE_NOT_FOUND) {
    Log(L"RegDeleteTree failed - %08x\n", ls);
    isOk = false;
  }

  return isOk;
}

bool ComRegistry::RegisterObject(LPCWSTR threadModel) {
  std::wstring subkey = kClsIdPrefix;
  subkey += RegUtil::GuidToString(clsId_);

  RegUtil root(GetHKey(ClassRoot), subkey.c_str());
  if (!root
      || !root.SetString(nullptr, name_)
      || !root.SetString(L"AppID", kPrevhost64))
    return false;

  RegUtil inproc(root, L"InprocServer32");
  if (!inproc
      || !inproc.SetString(nullptr, gDllPath)
      || !inproc.SetString(L"ThreadingModel", threadModel))
    return false;

  return true;
}

bool ComRegistry::RegisterExtensions() {
  const auto guidStr = RegUtil::GuidToString(clsId_);
  std::wstring subkey;

  for (const auto& extension : kExtensions) {
    subkey = extension.mSubkeyPrefix;
    if (subkey[subkey.size() - 1] == L'\\') {
      subkey += guidStr;
      RegUtil root(GetHKey(extension.mType), subkey.c_str());
      if (!root.SetString(nullptr, guidStr))
        return false;
    }
    else {
      RegUtil root(GetHKey(extension.mType), subkey.c_str());
      auto handlerName = root.GetString(nullptr);
      if (!handlerName.empty()) continue;

      if (!root.SetString(nullptr, guidStr))
        return false;
    }
  }

  for (const wchar_t* ext : kExts) {
    subkey = kPropHandlerList;
    subkey += ext;

    RegUtil root(GetHKey(Hive), subkey.c_str());
    if (!root.SetString(nullptr, guidStr.c_str())) {
      return false;
    }
  }

  RegUtil root(GetHKey(Hive), kPreviewList);
  if (!root.SetString(guidStr.c_str(), name_)) {
    return false;
  }

  return true;
}
