#include <memory>
#include <string>
#include <windows.h>
#include <shlwapi.h>
#include <shlobj.h>
#include <strsafe.h>

#include "res.h"

extern std::wstring gDllPath;
extern GUID CLSID_ExtZ;
extern void Log(LPCWSTR format, ...);

class IconExt : public IPersistFile,
                public IExtractIconA,
                public IExtractIconW {
  ULONG ref_;

public:
  IconExt() : ref_(1) {
    Log(L"IconExt: %p\n", this);
  }

  STDMETHODIMP QueryInterface(REFIID riid, void **ppv) {
    const QITAB QITable[] = {
      QITABENT(IconExt, IPersist),
      QITABENT(IconExt, IPersistFile),
      QITABENT(IconExt, IExtractIconA),
      QITABENT(IconExt, IExtractIconW),
      {0},
    };
    return QISearch(this, QITable, riid, ppv);
  }
  STDMETHODIMP_(ULONG) AddRef() { return InterlockedIncrement(&ref_); }
  STDMETHODIMP_(ULONG) Release() {
    auto cref = InterlockedDecrement(&ref_);
    if (cref == 0) {
      Log(L"Destroying IconExt %p\n", this);
      delete this;
    }
    return cref;
  }

  // IPersist
  STDMETHODIMP GetClassID(CLSID *pClassID) {
    *pClassID = CLSID_ExtZ;
    return S_OK;
  }

  // IPersistFile
  STDMETHODIMP GetCurFile(LPOLESTR *ppszFileName) { return E_NOTIMPL; }
  STDMETHODIMP IsDirty() { return S_FALSE; }
  STDMETHODIMP Load(LPCOLESTR pszFileName, DWORD dwMode) { return S_OK; }
  STDMETHODIMP Save(LPCOLESTR pszFileName, BOOL fRemember) { return E_NOTIMPL; }
  STDMETHODIMP SaveCompleted(LPCOLESTR pszFileName) { return E_NOTIMPL; }

  // IExtractIconA
  STDMETHODIMP Extract(PCSTR pszFile,
                       UINT nIconIndex,
                       HICON *phiconLarge,
                       HICON *phiconSmall,
                       UINT nIconSize) {
    return E_NOTIMPL;
  }

  STDMETHODIMP GetIconLocation(UINT uFlags,
                               PSTR pszIconFile,
                               UINT cchMax,
                               int* piIndex,
                               UINT* pwFlags) {
    return E_NOTIMPL;
  }

  // IExtractIconW
  STDMETHODIMP Extract(PCWSTR pszFile,
                       UINT nIconIndex,
                       HICON *phiconLarge,
                       HICON *phiconSmall,
                       UINT nIconSize) {
    return S_FALSE;
  }

  STDMETHODIMP GetIconLocation(UINT uFlags,
                               PWSTR pszIconFile,
                               UINT cchMax,
                               int* piIndex,
                               UINT* pwFlags) {
    if (cchMax <= gDllPath.size()) {
      return ERROR_INSUFFICIENT_BUFFER;
    }
    gDllPath.copy(pszIconFile, gDllPath.size());
    pszIconFile[gDllPath.size()] = 0;
    *pwFlags = GIL_DONTCACHE;
    *piIndex = -IDI_ICON2;
    return S_OK;
  }
};

IExtractIconW* CreateIconExt() {
  return new IconExt;
}
