#include <memory>
#include <windows.h>
#include <shlwapi.h>
#include <shlobj.h>
#include <strsafe.h>

#include "res.h"

extern HMODULE gThisModule;
extern UINT gRefCount;

extern void Log(LPCWSTR format, ...);

class MetaExt : public IInitializeWithStream,
                public IPropertyStore,
                public IPropertySetStorage {
  ULONG ref_;

public:
  MetaExt() : ref_(1) {
    Log(L"MetaExt: %p\n", this);
  }

  STDMETHODIMP QueryInterface(REFIID riid, void **ppv) {
    const QITAB QITable[] = {
      QITABENT(MetaExt, IInitializeWithStream),
      QITABENT(MetaExt, IPropertyStore),
      QITABENT(MetaExt, IPropertySetStorage),
      {0},
    };
    return QISearch(this, QITable, riid, ppv);
  }
  STDMETHODIMP_(ULONG) AddRef() { return InterlockedIncrement(&ref_); }
  STDMETHODIMP_(ULONG) Release() {
    auto cref = InterlockedDecrement(&ref_);
    if (cref == 0) {
      Log(L"Destroying MetaExt %p\n", this);
      delete this;
    }
    return cref;
  }

  // IInitializeWithStream
  STDMETHODIMP Initialize(IStream *pstream, DWORD grfMode) {
    return S_OK;
  }

  // IPropertyStore
  STDMETHODIMP GetCount(DWORD *cProps) {
    Log(L"IPropertyStore::GetCount\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP GetAt(DWORD iProp, PROPERTYKEY *pkey) {
    Log(L"IPropertyStore::GetAt\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP GetValue(REFPROPERTYKEY key, PROPVARIANT *pv) {
    Log(L"IPropertyStore::GetValue\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP SetValue(REFPROPERTYKEY key, REFPROPVARIANT propvar) {
    Log(L"IPropertyStore::SetValue\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP Commit() {
    Log(L"IPropertyStore::Commit\n");
    return E_NOTIMPL;
  }

  // IPropertySetStorage
  STDMETHODIMP Create(REFFMTID rfmtid,
                      const CLSID *pclsid,
                      DWORD grfFlags,
                      DWORD grfMode,
                      IPropertyStorage **ppprstg) {
    Log(L"IPropertySetStorage::Create\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP Open(REFFMTID rfmtid, DWORD grfMode, IPropertyStorage **ppprstg) {
    Log(L"IPropertySetStorage::Open\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP Delete(REFFMTID rfmtid) {
    Log(L"IPropertySetStorage::Delete\n");
    return E_NOTIMPL;
  }
  STDMETHODIMP Enum(IEnumSTATPROPSETSTG **ppenum) {
    Log(L"IPropertySetStorage::Enum\n");
    return E_NOTIMPL;
  }
};

IPropertyStore* CreateMetaExt() {
  return new MetaExt;
}
