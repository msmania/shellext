#include <windows.h>
#include <shlwapi.h>
#include <shlobj.h>
#include <strsafe.h>

extern void Log(LPCWSTR format, ...);

class PreviewExt : public IInitializeWithStream,
                   public IObjectWithSite,
                   public IOleWindow,
                   public IPreviewHandler {
  ULONG ref_;

public:
  PreviewExt() : ref_(1) {
    Log(L"PreviewExt: %p\n", this);
  }

  STDMETHODIMP QueryInterface(REFIID riid, void **ppv) {
    const QITAB QITable[] = {
      QITABENT(PreviewExt, IInitializeWithStream),
      QITABENT(PreviewExt, IObjectWithSite),
      QITABENT(PreviewExt, IOleWindow),
      QITABENT(PreviewExt, IPreviewHandler),
      {0},
    };
    return QISearch(this, QITable, riid, ppv);
  }
  STDMETHODIMP_(ULONG) AddRef() { return InterlockedIncrement(&ref_); }
  STDMETHODIMP_(ULONG) Release() {
    auto cref = InterlockedDecrement(&ref_);
    if (cref == 0) {
      Log(L"Destroying PreviewExt %p\n", this);
      delete this;
    }
    return cref;
  }

  // IInitializeWithStream
  STDMETHODIMP Initialize(IStream *pstream, DWORD grfMode) {
    return S_OK;
  }

  // IObjectWithSite
  STDMETHODIMP SetSite(IUnknown *pUnkSite) { return E_NOTIMPL; }
  STDMETHODIMP GetSite(REFIID riid, void **ppvSite) { return E_NOTIMPL; }

  // IOleWindow
  STDMETHODIMP GetWindow(HWND *phwnd) { return E_NOTIMPL; }
  STDMETHODIMP ContextSensitiveHelp(BOOL fEnterMode) { return E_NOTIMPL; }

  // IPreviewHandler
  STDMETHODIMP SetWindow( HWND hwnd, const RECT *prc) { return E_NOTIMPL; }
  STDMETHODIMP SetRect(const RECT *prc) { return E_NOTIMPL; }
  STDMETHODIMP DoPreview() { return E_NOTIMPL; }
  STDMETHODIMP Unload() { return E_NOTIMPL; }
  STDMETHODIMP SetFocus() { return E_NOTIMPL; }
  STDMETHODIMP QueryFocus(HWND *phwnd) { return E_NOTIMPL; }
  STDMETHODIMP TranslateAccelerator(MSG *pmsg) { return E_NOTIMPL; }
};

IPreviewHandler* CreatePreviewExt() { return new PreviewExt; }
