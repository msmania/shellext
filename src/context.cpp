#include <memory>
#include <windows.h>
#include <shlwapi.h>
#include <shlobj.h>
#include <strsafe.h>

extern void Log(LPCWSTR format, ...);

class ContextMenuExt : public IShellExtInit, public IContextMenu {
  ULONG ref_;

public:
  ContextMenuExt();

  STDMETHODIMP QueryInterface(REFIID riid, void **ppv);
  STDMETHODIMP_(ULONG) AddRef();
  STDMETHODIMP_(ULONG) Release();

  // IShellExtInit
  STDMETHODIMP Initialize(PCIDLIST_ABSOLUTE pidlFolder,
                          IDataObject *pdtobj,
                          HKEY hkeyProgID);

  // IContextMenu
  STDMETHODIMP QueryContextMenu(HMENU hmenu,
                                UINT indexMenu,
                                UINT idCmdFirst,
                                UINT idCmdLast,
                                UINT uFlags);

  STDMETHODIMP InvokeCommand(CMINVOKECOMMANDINFO *pici);

  STDMETHODIMP GetCommandString(UINT_PTR idCmd,
                                UINT uType,
                                UINT *pReserved,
                                CHAR *pszName,
                                UINT cchMax);
};

ContextMenuExt::ContextMenuExt() : ref_(1) {
  Log(L"ContextMenuExt: %p\n", this);
}

STDMETHODIMP ContextMenuExt::QueryInterface(REFIID riid, void **ppv) {
  const QITAB QITable[] = {
    QITABENT(ContextMenuExt, IShellExtInit),
    QITABENT(ContextMenuExt, IContextMenu),
    {0},
  };
  return QISearch(this, QITable, riid, ppv);
}

STDMETHODIMP_(ULONG) ContextMenuExt::AddRef() {
  return InterlockedIncrement(&ref_);
}

STDMETHODIMP_(ULONG) ContextMenuExt::Release() {
  auto cref = InterlockedDecrement(&ref_);
  if (cref == 0) {
    Log(L"Destroying ContextMenuExt %p\n", this);
    delete this;
  }
  return cref;
}

void PrintInfo(PCIDLIST_ABSOLUTE pidlFolder, IDataObject *pdtobj, HKEY hkeyProgID) {
  HRESULT hr;

  if (pidlFolder) {
    wchar_t folderName[MAX_PATH];
    hr = SHGetPathFromIDListEx(pidlFolder, folderName, MAX_PATH,
                                       GPFIDL_DEFAULT | GPFIDL_UNCPRINTER);
    if (SUCCEEDED(hr)) {
      Log(L"  Folder: %s\n", folderName);
    }
    else {
      Log(L"SHGetPathFromIDListEx failed - %08x\n", hr);
    }
  }

  if (pdtobj) {
    STGMEDIUM medium;
    FORMATETC fe = {CF_HDROP, nullptr, DVASPECT_CONTENT, -1, TYMED_FILE};
    hr = pdtobj->GetData(&fe, &medium);

    if (hr == DV_E_FORMATETC) {
      medium = {TYMED_HGLOBAL};
      fe = {CF_HDROP, nullptr, DVASPECT_CONTENT, -1, TYMED_HGLOBAL};
      hr = pdtobj->GetData(&fe, &medium);
    }

    if (SUCCEEDED(hr)) {
      switch (medium.tymed) {
      case TYMED_FILE:
        Log(L"  TYMED_FILE: %s\n", medium.lpszFileName);
        break;
      case TYMED_HGLOBAL:
        if (HDROP drop = reinterpret_cast<HDROP>(GlobalLock(medium.hGlobal))) {
          WCHAR path[MAX_PATH];
          if (::DragQueryFileW(drop, 0, path, MAX_PATH)) {
            Log(L"  TYMED_HGLOBAL: %s\n", path);
          }
          GlobalUnlock(medium.hGlobal);
        }
        break;
      }

      ReleaseStgMedium(&medium);
    }
    else {
      Log(L"  IDataObject::GetData failed - %08x\n", hr);
    }
  }

  DWORD len;
  RegGetValueW(hkeyProgID, nullptr, nullptr, RRF_RT_REG_SZ, nullptr, nullptr, &len);
  std::unique_ptr<uint8_t[]> buf(new uint8_t[len]);
  LSTATUS ls = RegGetValueW(hkeyProgID, nullptr, nullptr,
                            RRF_RT_REG_SZ, nullptr, buf.get(), &len);
  if (ls == ERROR_SUCCESS) {
    Log(L"  ProgID: %s\n", reinterpret_cast<const wchar_t*>(buf.get()));
  }
}

STDMETHODIMP ContextMenuExt::Initialize(PCIDLIST_ABSOLUTE pidlFolder,
                                        IDataObject *pdtobj,
                                        HKEY hkeyProgID) {
  PrintInfo(pidlFolder, pdtobj, hkeyProgID);
  return S_OK;
}

STDMETHODIMP ContextMenuExt::QueryContextMenu(HMENU hmenu,
                                              UINT indexMenu,
                                              UINT idCmdFirst,
                                              UINT idCmdLast,
                                              UINT uFlags) {
  wchar_t label[] = L":)";

  MENUITEMINFO item = {sizeof(MENUITEMINFO)};
  item.fMask = MIIM_FTYPE | MIIM_ID | MIIM_STATE | MIIM_STRING;
  item.fType = MFT_STRING;
  item.fState = MFS_ENABLED;
  item.wID = idCmdFirst;
  item.dwTypeData = label;
  item.cch = ARRAYSIZE(label) - 1;

  if (!InsertMenuItem(hmenu, indexMenu, /*fByPosition*/TRUE, &item)) {
    DWORD gle = GetLastError();
    Log(L"InsertMenuItem failed - %08lx\n", gle);
    return HRESULT_FROM_WIN32(gle);
  }

  return MAKE_HRESULT(SEVERITY_SUCCESS, 0, 1);
}

STDMETHODIMP ContextMenuExt::InvokeCommand(CMINVOKECOMMANDINFO *pici) {
  Log(L"ContextMenuExt::InvokeCommand\n");
  return S_OK;
}

STDMETHODIMP ContextMenuExt::GetCommandString(UINT_PTR idCmd,
                                              UINT uType,
                                              UINT *pReserved,
                                              CHAR *pszName,
                                              UINT cchMax) {
  Log(L"ContextMenuExt::GetCommandString\n");
  return S_OK;
}

IContextMenu* CreateContextMenuExt() {
  return new ContextMenuExt;
}
