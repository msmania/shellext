#include <windows.h>
#include <shlwapi.h>
#include <shlobj.h>
#include <strsafe.h>

extern void Log(LPCWSTR format, ...);

class CopyExt : public ICopyHookW {
  ULONG ref_;

public:
  CopyExt() : ref_(1) {
    Log(L"CopyExt: %p\n", this);
  }

  STDMETHODIMP QueryInterface(REFIID riid, void **ppv) {
    const QITAB QITable[] = {
      QITABENT(CopyExt, ICopyHookW),
      {0},
    };
    return QISearch(this, QITable, riid, ppv);
  }
  STDMETHODIMP_(ULONG) AddRef() { return InterlockedIncrement(&ref_); }
  STDMETHODIMP_(ULONG) Release() {
    auto cref = InterlockedDecrement(&ref_);
    if (cref == 0) {
      Log(L"Destroying CopyExt %p\n", this);
      delete this;
    }
    return cref;
  }

  // ICopyHook
  STDMETHODIMP_(UINT) CopyCallback(HWND hwnd,
                                   UINT wFunc,
                                   UINT wFlags,
                                   PCWSTR pszSrcFile,
                                   DWORD dwSrcAttribs,
                                   LPCWSTR pszDestFile,
                                   DWORD dwDestAttribs) {
    Log(L"CopyCallback: %p %s -> %s\n",
        this, pszSrcFile, pszDestFile);
    return IDYES;
  }
};

IUnknown* CreateCopyExt() { return new CopyExt; }
