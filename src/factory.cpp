#include <atlbase.h>
#include <windows.h>
#include <shlwapi.h>
#include <shlobj.h>
#include <thumbcache.h>

extern void Log(LPCWSTR format, ...);

IUnknown* CreateOverlayIconExt();
IContextMenu* CreateContextMenuExt();
IUnknown* CreateColumnExt();
IUnknown* CreateCopyExt();
IShellPropSheetExt* CreatePropExt();
IDataObject* CreateDataExt();
IDropTarget* CreateDropExt();
IExtractIconW* CreateIconExt();
IPreviewHandler* CreatePreviewExt();
IThumbnailProvider* CreateThumbExt();
IQueryInfo* CreateInfotipExt();
IPropertyStore* CreateMetaExt();

class ClassFactory : public IClassFactory {
  ULONG ref_;

public:
  ClassFactory();

  // IUnknown
  STDMETHODIMP QueryInterface(REFIID riid, void **ppv);
  STDMETHODIMP_(ULONG) AddRef();
  STDMETHODIMP_(ULONG) Release();

  // IClassFactory
  STDMETHODIMP CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv);
  STDMETHODIMP LockServer(BOOL fLock);
};

ClassFactory::ClassFactory() : ref_(1) {
  // Log(L"ClassFactory: %p\n", this);
}

STDMETHODIMP ClassFactory::QueryInterface(REFIID riid, void **ppv) {
  const QITAB QITable[] = {
    QITABENT(ClassFactory, IClassFactory),
    {0},
  };
  return QISearch(this, QITable, riid, ppv);
}

STDMETHODIMP_(ULONG) ClassFactory::AddRef() {
  return InterlockedIncrement(&ref_);
}

STDMETHODIMP_(ULONG) ClassFactory::Release() {
  auto cref = InterlockedDecrement(&ref_);
  if (cref == 0) {
    // Log(L"Destroying ClassFactory %p\n", this);
    delete this;
  }
  return cref;
}

STDMETHODIMP ClassFactory::CreateInstance(IUnknown *pUnkOuter,
                                          REFIID riid,
                                          void **ppv) {
  if (pUnkOuter) return CLASS_E_NOAGGREGATION;

  CComPtr<IUnknown> newInstance;
  if (IsEqualCLSID(riid, IID_IShellIconOverlayIdentifier)) {
    newInstance.Attach(CreateOverlayIconExt());
  }
  else if (IsEqualCLSID(riid, IID_IContextMenu)) {
    newInstance.Attach(CreateContextMenuExt());
  }
  else if (IsEqualCLSID(riid, IID_ICopyHookW)) {
    newInstance.Attach(CreateCopyExt());
  }
  else if (IsEqualCLSID(riid, IID_IShellPropSheetExt)) {
    newInstance.Attach(CreatePropExt());
  }
  else if (IsEqualCLSID(riid, IID_IDataObject)) {
    newInstance.Attach(CreateDataExt());
  }
  else if (IsEqualCLSID(riid, IID_IDropTarget)) {
    newInstance.Attach(CreateDropExt());
  }
  else if (IsEqualCLSID(riid, IID_IExtractIconA)
           || IsEqualCLSID(riid, IID_IExtractIconW)) {
    newInstance.Attach(CreateIconExt());
  }
  else if (IsEqualCLSID(riid, IID_IPreviewHandler)) {
    newInstance.Attach(CreatePreviewExt());
  }
  else if (IsEqualCLSID(riid, IID_IThumbnailProvider)) {
    newInstance.Attach(CreateThumbExt());
  }
  else if (IsEqualCLSID(riid, IID_IQueryInfo)) {
    newInstance.Attach(CreateInfotipExt());
  }
  else if (IsEqualCLSID(riid, IID_IPropertyStore)
           || IsEqualCLSID(riid, IID_IPropertySetStorage)) {
    newInstance.Attach(CreateMetaExt());
  }
  else {
    LPWSTR guidStr = nullptr;
    if (SUCCEEDED(StringFromCLSID(riid, &guidStr))) {
      Log(L"QI: %s\n", guidStr);
      __debugbreak();
      CoTaskMemFree(guidStr);
    }
    return E_NOINTERFACE;
  }

  return newInstance ? newInstance->QueryInterface(riid, ppv)
                     : E_OUTOFMEMORY;
}

STDMETHODIMP ClassFactory::LockServer(BOOL fLock) {
  return S_OK;
}

IUnknown* CreateZzzFactory() {
  return new ClassFactory;
}
