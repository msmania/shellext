#include <windows.h>
#include <shlwapi.h>
#include <shlobj.h>
#include <strsafe.h>

extern void Log(LPCWSTR format, ...);

class ColumnExt : public IColumnProvider {
  ULONG ref_;

public:
  ColumnExt() : ref_(1) {
    Log(L"ColumnExt: %p\n", this);
  }

  STDMETHODIMP QueryInterface(REFIID riid, void **ppv) {
    const QITAB QITable[] = {
      QITABENT(ColumnExt, IColumnProvider),
      {0},
    };
    return QISearch(this, QITable, riid, ppv);
  }
  STDMETHODIMP_(ULONG) AddRef() { return InterlockedIncrement(&ref_); }
  STDMETHODIMP_(ULONG) Release() {
    auto cref = InterlockedDecrement(&ref_);
    if (cref == 0) {
      Log(L"Destroying ColumnExt %p\n", this);
      delete this;
    }
    return cref;
  }

  // IColumnProvider
  STDMETHODIMP Initialize(LPCSHCOLUMNINIT psci) {
    Log(L"ColumnExt::Initialize(%s)\n", psci->wszFolder);
    return E_NOTIMPL;
  }
  STDMETHODIMP GetColumnInfo(DWORD dwIndex, SHCOLUMNINFO *psci) {
    return E_NOTIMPL;
  }
  STDMETHODIMP GetItemData(LPCSHCOLUMNID pscid,
                           LPCSHCOLUMNDATA pscd, 
                           VARIANT *pvarData) {
    return E_NOTIMPL;
  }
};

IUnknown* CreateColumnExt() { return new ColumnExt; }
